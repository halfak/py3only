import platform
import sys
if sys.version_info <= (3, 0):
    from pkg_resources import VersionConflict
    raise VersionConflict("py3only requires Python '>=3' but the running Python is " + platform.python_version())
