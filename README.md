## Python 3 Only

This package requires python 3 only.  It's useful for running tests.  
It was inspired by a discussion like
https://phabricator.wikimedia.org/T181693
