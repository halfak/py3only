import platform
import sys
if sys.version_info <= (3, 0):
    print("py3only requires Python '>=3' but the running Python is " + platform.python_version())
    sys.exit(1)

import os
import re

from setuptools import find_packages, setup


setup(
    name="py3only",
    version="0.0.1",  # noqa
    author="Aaron Halfaker",  # noqa
    author_email="aaron.halfaker@gmail.com",  # noqa
    description="This package requires python 3 and does nothing.",  # noqa
    url=None,  # noqa
    license="MIT License",  # noqa
    packages=find_packages(),
    include_package_data=True,
    long_description="See the short description.",
    install_requires=[],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
    ],
    python_requires=">=3"
)
